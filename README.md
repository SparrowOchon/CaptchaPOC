# CaptchaPOC

POC of a Captcha Solver to use in correlation with a script to automate tasks.
The captchas are numeric only and official source not mentioned to prevent spam.

# How

The solver uses a Convolutional Neural Network that has been custom trained with
samples of the captcha from the official source. It has a 92% Accuracy rating
on live data from the official source.

# Installation

Packages are frozen with requirements.txt
`pip3 install -r requirements.txt`

# Usage

Solver currently is just a test with the currently downloaded sample data in
`Test-data` and can be executed by simply calling the script `python3 solve.py`
