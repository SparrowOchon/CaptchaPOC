from keras.models import load_model
from helpers import resizeImage
from imutils import paths
import numpy
import imutils
import cv2
import pickle


CAPTCHA_IMAGE_FOLDER = "Test-Data"
MODEL_FILENAME = "captcha_model.hdf5"
MODEL_LABELS_FILENAME = "model_labels.dat"


# Load Dataset
with open(MODEL_LABELS_FILENAME, "rb") as f:
    loadDateMapping = pickle.load(f)

# Load Neural network
neuralNetwork = load_model(MODEL_FILENAME)

# Pick random entry from Test-Data to test Against
testCaptchas = numpy.random.choice(list(paths.list_images(CAPTCHA_IMAGE_FOLDER)), size=(10,), replace=False)

# loop over the TestCaptchas
for captchaImage in testCaptchas:
    # Image Preprocessing
    image = cv2.imread(captchaImage)
    morph = image.copy()
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)
    morph = cv2.morphologyEx(morph, cv2.MORPH_OPEN, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
    gradient = cv2.morphologyEx(morph, cv2.MORPH_GRADIENT, kernel)
    imageRGB = numpy.split(numpy.asarray(gradient), 3, axis=2)
    channelHeight, channelWidth, _ = imageRGB[0].shape
    for i in range(0, 3):
        _, imageRGB[i] = cv2.threshold(~imageRGB[i], 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY)
        imageRGB[i] = numpy.reshape(imageRGB[i], newshape=(channelHeight, channelWidth, 1))
    image = numpy.concatenate((imageRGB[0], imageRGB[1], imageRGB[2]), axis=2)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.copyMakeBorder(gray, 30, 30, 30, 30, cv2.BORDER_REPLICATE)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    # Find each Character
    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[1]

    digitLocations = []

    # Try to Identify each digits Position
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour)
        if w / h > 1.25:                        # Split if Characters 2 Close
            halfWidth = int(w / 2)
            digitLocations.append((x, y, halfWidth, h))
            digitLocations.append((x + halfWidth, y, halfWidth, h))
        elif h >= 2*w:                          # Remove Bleed
            modifiedHeight = int(w + 4)
            digitLocations.append((x, y, w, modifiedHeight))
        else:
            # This is a normal letter by itself
            digitLocations.append((x, y, w, h))


    # Check if we detected all 9 Digits
    if len(digitLocations) != 9:
        continue

    # Sort the detected digit from X left to Right 
    digitLocations = sorted(digitLocations, key=lambda x: x[0])

    # Create an output image and a list to hold our predicted digits
    output = cv2.merge([gray] * 3)
    predictions = []

    # loop over the Digits
    for digitCordinate in digitLocations:
        # Grab the coordinates of the digit in the image
        x, y, w, h = digitCordinate

        # Extract the digit from the original image with a 2-pixel margin around the edge
        digit = gray[y - 2:y + h + 2, x - 2:x + w + 2]
        digit = resizeImage(digit, 20, 20)

        # Turn the single image into a 4d list of images to make Keras happy
        digit = numpy.expand_dims(digit, axis=2)
        digit = numpy.expand_dims(digit, axis=0)

        # Make Prediction on letter
        prediction = neuralNetwork.predict(digit)

        # Convert the prediction back to a normal letter
        digit = loadDateMapping.inverse_transform(prediction)[0]
        predictions.append(digit)

        # draw the prediction on the output image
        cv2.rectangle(output, (x - 2, y - 2), (x + w + 4, y + h + 4), (0, 255, 0), 1)
        cv2.putText(output, digit, (x - 5, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

    # Print the captcha's text
    captchaDigit = "".join(predictions)
    print("CAPTCHA text is:" + captchaDigit +"\n")

    # Show the annotated image
    cv2.imshow("Output", output)
    cv2.waitKey()