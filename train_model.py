import cv2
import _pickle as pickle
import os.path
import numpy
from imutils import paths
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.core import Flatten, Dense
from helpers import resizeImage


DIGIT_IMAGES_FOLDER = "ExtractedDigits"
MODEL_FILENAME = "captcha_model.hdf5"
MODEL_LABELS_FILENAME = "model_labels.dat"


# initialize the data and labels
digitData = []
digitLabels = []

# loop over the input images
for digit in paths.list_images(DIGIT_IMAGES_FOLDER):
  
    image = cv2.imread(digit)
    image = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
    image = resizeImage(image, 20, 20)
    image = numpy.expand_dims(image, axis=2)
    label = digit.split(os.path.sep)[-2]
    digitData.append(image)
    digitLabels.append(label)


# scale the raw pixel intensities to the range [0, 1]
digitData = numpy.array(digitData, dtype="float") / 255.0
digitLabels = numpy.array(digitLabels)

# Split the training data into separate train and test sets
(X_train, X_test, Y_train, Y_test) = train_test_split(data, labels, test_size=0.25, random_state=0)

# Convert the labels (letters) into one-hot encodings that Keras can work with
lb = LabelBinarizer().fit(Y_train)
Y_train = lb.transform(Y_train)
Y_test = lb.transform(Y_test)

# Save the mapping from labels to one-hot encodings.
# We'll need this later when we use the model to decode the prediction
with open(MODEL_LABELS_FILENAME, "wb") as f:
    pickle.dump(lb, f)

# Build the neural network!
model = Sequential()

# First convolutional layer with max pooling
model.add(Conv2D(32, (5, 5), padding="same", input_shape=(20, 20, 1), activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

# Second convolutional layer with max pooling
model.add(Conv2D(64, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

# Hidden layer with 500 nodes
model.add(Flatten())
model.add(Dense(500, activation="relu"))

# Output layer with 10 nodes (one for each possible number we predict)
model.add(Dense(10, activation="softmax"))

# Ask Keras to build the TensorFlow model behind the scenes
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Train the neural network
model.fit(X_train, Y_train, batch_size=10, epochs=30, verbose=1, validation_data=(X_test, Y_test))

# Save the trained model to disk
model.save(MODEL_FILENAME)
